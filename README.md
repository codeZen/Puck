# Puck

My functional PHP toolkit of choice (WIP)

Puck is still in development (see dev branch) but if you want to grab it and give it a test-drive:
1. Tell Composer where to find the Puck repo:
```
❱ composer config repositories.codezen/puck git https://codeberg.org/codeZen/Puck.git
```
2. Install directly from the dev branch
```
❱ composer require codezen/puck:dev-dev
```
3. See `/lib` folder for core functions and collection types

<br>
Full documentation will be added upon v1.0 release